<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/spip-bonux.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// S
	'spipcli_runner_description' => 'Un plugin pour faciliter l\'utilisation de SPIP-Cli depuis SPIP',
	'spipcli_runner_slogan' => 'Inception'
);
