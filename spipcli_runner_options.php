<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function spipcli_allowed(string $command) {
	if (!defined('_SPIPCLI_BIN_PATH')) {
		define('_SPIPCLI_BIN_PATH', 'spip');
	}

	if (defined('_SPIPCLI_FORBIDDEN_COMMANDS')) {
		if (in_array($command, _SPIPCLI_FORBIDDEN_COMMANDS)) {
			return false;
		}
	}

	if (defined('_SPIPCLI_ALLOWED_COMMANDS')) {
		return in_array($command, _SPIPCLI_ALLOWED_COMMANDS);
	}

	return true;
}

/**
 * @param string $command
 * @param array $args
 * @param array $options
 * @param int $return_var
 * @return ?array
 */
function spipcli_run(string $command, array $args = [], array $options = [], &$result_code = null): ?array {

	if (!$command or !preg_match(',^[\w-]+(:[\w-]+)*$,', $command)) {
		spip_log("spipcli_run: command $command mal formée", 'spipcli' . _LOG_ERREUR);
		$result_code = 256;
		return null;
	}

	if (!spipcli_allowed($command)) {
		spip_log("spipcli_run: command $command non autorisée", 'spipcli' . _LOG_ERREUR);
		$result_code = 128;
		return null;
	}

	// il faut être à la racine du site pour executer spip-cli
	$current_dir = getcwd();
	if (_DIR_RACINE) {
		chdir(_ROOT_RACINE);
	}
	$spipcli_runner_exec = charger_fonction('spipcli_runner_exec', 'inc');
	$res = $spipcli_runner_exec($command, $args, $options, $result_code);

	// et on se remets dans le répertoire où on était juste avant
	chdir($current_dir);

	return $res;
}