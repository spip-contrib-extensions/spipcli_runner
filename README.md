# SPIP-Cli Runner

Ce plugin est un helper pour lancer facilement des commandes spip-cli depuis des actions utilisateur dans SPIP.
Voir aussi l'outil SPIP-Cli https://git.spip.net/spip-contrib-outils/spip-cli

## Utilisation

La fonction helper `spipcli_run` permet de lancer facilement la commande.
La fonction retourne directement le résultat affiché par la commande, sous forme de tableau avec une entrée par ligne.
La fonction se charge des échappements de sécurité sur les arguments et options.

```php
$res = spipcli_run('jobs:lister', [], ['--past']);
$res = implode("\n", $res);
echo $res . "\n";
```

Il est possible de passer en 4ème argument une variable `$result_code` pour récupérer un code d'erreur en cas d'echec.


## Constantes

### `_SPIPCLI_FORBIDDEN_COMMANDS`

La constante `_SPIPCLI_FORBIDDEN_COMMANDS` permet d'interdire l'appel de certaines commandes.
Si elle est définie, les commandes listées par cette constante seront interdites et l'appel à `spipcli_run` échouera, renvoyant un `null`

### `_SPIPCLI_ALLOWED_COMMANDS`

La constante `__SPIPCLI_ALLOWED_COMMANDS` permet d'autoriser explicitement l'appel de certaines commandes.
Si elle est définie, **seules** les commandes listées par cette constante seront autorisées, et toute autre commande échouera, renvoyant un `null`


### `_SPIPCLI_BIN_PATH`

La constante `_SPIPCLI_BIN_PATH` permet de définir le chemin vers l'executable `spip`.
Par défait on utilisera simplement `spip` en supposant que l'executable est dans le path

## Personalisation

La fonction surchargeable `inc_spipcli_runner_exec_dist()` prend en charge l'appel final à spip-cli, via une commande `exec` par défaut.

Dans en environnement serveur web que l'on veut sécuriser, on pourra interdire la fonction PHP `exec()` (et ses variantes…),
et surcharger la fonction pour fournir le service de manière alternative,
permettant ainsi aux scripts SPIP d'accéder à un jeu réduit et encadré de commandes cli (notamment via la constante `SPIPCLI_ALLOWED_COMMANDS`)

## Commande `allow`

Le plugin ajoute une commande `allow` à SPIP-Cli pour vérifier l'autorisation d'executer une commande cli d'après les define éventuels du plugin.
La commande `spip allow [...]` renvoie un code d'erreur si la commande testée n'est pas permise.

Il est ainsi possible de lancer toutes les commandes spip-cli sous cette forme :
`spip allow jobs:purger && spip jobs:purger`

Si la commande spip-cli `jobs:purger` n'est pas permise par les constantes du plugin, la commande ne fera rien.
