<?php

namespace Spip\Cli\Command;

use Spip\Cli\Application;
use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Allow extends Command {
	protected function configure() {
		$this
			->setName('allow')
			->addArgument('name', InputArgument::REQUIRED, 'Le nom de la commande spip-cli à tester')
			->setDescription("Renvoie un code d'erreur si la commande cli n\'est pas permise.
			Permet de tester une commande avant de la lancer : `spip allow php:eval && spip php:eval 'die()'")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		$command = $input->getArgument('name');
		if (
			!$this->getApplication()->has($command)
			|| !spipcli_allowed($command)
		) {
			return self::FAILURE;
		}

		return self::SUCCESS;
	}
}
