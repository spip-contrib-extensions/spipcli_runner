<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * @param string $spip_cli_command
 * @param array $args
 * @param array $options
 * @param int $return_var
 * @return ?array
 */
function inc_spipcli_runner_exec_dist(string $spip_cli_command, array $args = [], array $options = [], &$result_code = null): ?array {

	$output = [];

	// $spip_cli_command peut etre préfixé d'un path pour demander un chdir avant execution
	$chdir = '';
	if (strpos($spip_cli_command, '/') !== false) {
		$chdir = explode('/', $spip_cli_command);
		$spip_cli_command = array_pop($chdir);
		$chdir = implode('/', $chdir);
		$chdir = "cd $chdir && ";
	}
	// mais uniquement si on a le bon define...
	if (!defined("_SPIPCLI_RUNNER_ALLOW_CHDIR") or !_SPIPCLI_RUNNER_ALLOW_CHDIR) {
		$chdir = "";
	}

	$command = $chdir . _SPIPCLI_BIN_PATH . ' ' . $spip_cli_command;

	foreach ($args as $arg) {
		if (!empty($arg)) {
			$command .= ' ' . escapeshellarg($arg);
		}
	}

	foreach ($options as $option_name => $option_value) {
		if (is_numeric($option_name)) {
			$option_name = $option_value;
			$option_value = null;
		}
		if (preg_match(',^--[\w-]+$,', $option_name)) {
			$command .= ' ' . $option_name;
			if ($option_value !== null) {
				$command .= '=' . escapeshellarg($option_value);
			}
		}
	}

	spip_log('spipcli_run : ' . $command, 'spipcli' . _LOG_DEBUG);
	exec($command, $output, $result_code);

	return $output;
}
